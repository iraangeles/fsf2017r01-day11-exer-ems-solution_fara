// TODO: Retrieve (retrieve all and eager loading) and insert data from the database.
// TODO: 3.2 Define dept_emp table model
// Model for dept_emp table
// References:
// http://docs.sequelizejs.com/en/latest/docs/getting-started/#your-first-model
// http://docs.sequelizejs.com/en/latest/docs/models-definition/
module.exports = function (sequelize, Sequelize) {
    var DeptEmp = sequelize.define("dept_emp",
        {
            emp_no: {
                type: Sequelize.INTEGER(11),
                primaryKey: true,
                allowNull: false,
                references: {
                    model: 'employees',
                    key: 'emp_no'
                }
            },
            dept_no: {
                type: Sequelize.STRING,
                primaryKey: true,
                allowNull: false,
                references: {
                    model: 'departments',
                    key: 'dept_no'
                }
            },
            from_date: {
                type: Sequelize.DATE,
                allowNull: false
            },
            to_date: {
                type: Sequelize.DATE,
                allowNull: false
            }
        },
        {
            // don't add timestamps attributes updatedAt and createdAt
            timestamps: false
            // disable the modification of table names; By default, sequelize will automatically
            // transform all passed model names (first parameter of define) into plural.
            // if you don't want that, set the following
            , freezeTableName: true
            , tableName: 'dept_emp'
         });

    return DeptEmp;
};