// Always use an IIFE, i.e., (function() {})();
(function() {
    angular
        .module("EMS")          // to call an angular module, omit the second argument ([]) from the angular.module() syntax
        // this syntax is called the getter syntax
        .controller("RegCtrl", RegCtrl);    // angular.controller() attaches a controller to the angular module specified
                                            // as you can see, angular methods are chainable

    // Dependency injection. An empty [] means RegCtrl does not have dependencies. Here we inject EmpService so
    // RegCtrl can call services related to employees.
    RegCtrl.$inject = ['$window', 'EmpService'];

    // RegCtrl function declaration. A function declaration uses the syntax: functionName([arg [, arg [...]]]){ ... }
    // RegCtrl accepts the injected dependency as a parameter. We name it EmpService for consistency, but you may
    // assign any name
    function RegCtrl($window, EmpService) {

        // Declares the var vm (for ViewModel) and assigns it the object this (in this case, the RegCtrl)
        // Any function or variable that you attach to vm will be exposed to callers of RegCtrl, e.g., index.html
        var vm = this;
        var today = new Date();
        var birthday = new Date();
        birthday.setFullYear(birthday.getFullYear() - 18);

        // Exposed data models ---------------------------------------------------------------------------------------
        // Creates an employee object that
        // We expose the employee object by attaching it to the vm
        // This will allow us apply two-way data-binding to this object by using ng-model in our view (i.e., index.html)
        vm.employee = {
            empNo: "",
            firstname: "",
            lastname: "",
            gender: "M",
            birthday: birthday,
            hiredate: today,
            phonenumber: ""
        };

        // Creates a status object. We will use this to display appropriate success or error messages.
        vm.status = {
            message: "",
            code: ""
        };

        // Exposed functions ------------------------------------------------------------------------------------------
        // Exposed functions can be called from the view. e.g., to call the vm.register from our view (index.html), code:
        // ctrl.register()
        vm.register = register;

        // Function declaration and definition
        function register() {
            // Calls alert box and displays registration information
            alert("The registration information you sent are \n" + JSON.stringify(vm.employee));

            // Prints registration information onto the client console
            console.log("The registration information you sent were:");
            console.log("Employee Number: " + vm.employee.empNo);
            console.log("Employee First Name: " + vm.employee.firstname);
            console.log("Employee Last Name: " + vm.employee.lastname);
            console.log("Employee Gender: " + vm.employee.gender);
            console.log("Employee Birthday: " + vm.employee.birthday);
            console.log("Employee Hire Date: " + vm.employee.hiredate);
            console.log("Employee Phone Number: " + vm.employee.phonenumber)

            // We call EmpService.insertEmp to handle registration of employee information. The data sent to this
            // function will eventually be inserted into the database.
            EmpService
                .insertEmp(vm.employee)
                .then(function (result) {
                    console.log("result " + JSON.stringify(result));
                    $window.location.assign('/app/registration/thanks.html');
                })
                .catch(function (err) {
                    console.log("error " + JSON.stringify(err));
                    vm.status.message = err.data.name;
                    vm.status.code = err.data.parent.errno;
                });

        } // END function register()
    } // END RegCtrl
})();